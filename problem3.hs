-- Problem 3
-- 13195 の素因数は 5、7、13、29 である。
-- 600851475143 の素因数のうち最大のものを求めよ。

import Data.Numbers.Primes (primeFactors)

main = do
  let n = 600851475143
  print . maximum . primeFactors $ n


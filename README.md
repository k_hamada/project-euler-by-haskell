# Challenge the Project Euler

## OverView
[Haskell](http://www.haskell.org/)の勉強のため、[Project Euler](http://projecteuler.net/)に挑戦。

日本語訳は[Project Euler - PukiWiki](http://odz.sakura.ne.jp/projecteuler/)を参照。

## Rule
1.  なるべく素直な書き方をする。
1.  解き方やアルゴリズムが分らない場合は検索する。
1.  答えそのものを調べない＆写さない。
1.  使えるコード片やモジュールなどは使う。

## Status
![profile image](http://projecteuler.net/profile/k.hamada.png)


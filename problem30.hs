-- Problem 30
-- 驚くべきことに, 各桁を4乗した数の和が元の数と一致する数は3つしかない.
--
--   1634 = 1^4 + 6^4 + 3^4 + 4^4
--   8208 = 8^4 + 2^4 + 0^4 + 8^4
--   9474 = 9^4 + 4^4 + 7^4 + 4^4
--
-- ただし, 1=1^4は含まないものとする. この数たちの和は 1634 + 8208 + 9474 = 19316 である.
--
-- 各桁を5乗した数の和が元の数と一致するような数の総和を求めよ.

import Data.Char (digitToInt)

problem30 :: Int -> Bool
problem30 n =
  n == (sum . map ((^5) . digitToInt) . show $ n)

main =
  print . sum . filter problem30 $ [2..(9^5 * 6)]
  -- http://pamgau.net/item/699


-- Problem 17
-- 1 から 5 までの数字を英単語で書けば one, two, three, four, five であり、全部で 3 + 3 + 5 + 4 + 4 = 19 の文字が使われている。
-- では 1 から 1000 (one thousand) までの数字をすべて英単語で書けば、全部で何文字になるか。
--
-- 注: 空白文字やハイフンを数えないこと。例えば、342 (three hundred and forty-two) は 23 文字、115 (one hundred and fifteen) は20文字と数える。なお、"and" を使用するのは英国の慣習。

import Data.Char (digitToInt)

one  n = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"] !! n
teen n = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"] !! n
ty   n = ["", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"] !! n

problem17 :: [Int] -> String
problem17 [0, 0, 0, o] = one o
problem17 [0, 0, 1, o] = teen o
problem17 [0, 0, t, o] = ty t ++ one o
problem17 [0, h, 0, 0] = one h ++ "hundred"
problem17 [0, h, t, o] = one h ++ "hundredand" ++ problem17 [0, 0, t, o]
problem17 [k, 0, 0, 0] = one k ++ "thousand"

intToArr :: Int -> [Int]
intToArr n =
  padding ++ splitInt
  where splitInt = map digitToInt . show $ n
        padding = replicate (4 - length splitInt) 0

main =
  print . sum . map ((length . problem17) . intToArr) $ [1..1000]


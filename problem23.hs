-- Problem 23
-- 完全数とは, その数の真の約数の和がそれ自身と一致する数のことである. たとえば, 28の真の約数の和は, 1 + 2 + 4 + 7 + 14 = 28であるので, 28は完全数である.
--
-- 真の約数の和がその数よりも少ないものを不足数といい, 真の約数の和がその数よりも大きいものを過剰数と呼ぶ.
--
-- 12は, 1+2+3+4+6=16となるので, 最小の過剰数である. よって2つの過剰数の和で書ける最少の数は24である. 数学的な解析により, 28123より大きい任意の整数は2つの過剰数の和で書けることが知られている. 2つの過剰数の和で表せない最大の数がこの上限よりも小さいことは分かっているのだが, この上限を減らすことが出来ていない.
--
-- 2つの過剰数の和で書き表せない正の整数の総和を求めよ.

-- http://stackoverflow.com/questions/1480563/making-a-list-of-divisors-in-haskell
import Data.List (nub)
divisors' n = (1:) $ nub $ concat [ [x, div n x] | x <- [2..limit], rem n x == 0 ]
     where limit = (floor.sqrt.fromIntegral) n

isAbundant n = let m = sum . divisors' $ n in n < m

abudant = filter isAbundant [1..28123]

isAbuDouble n =
  any (\i -> (n - i) `elem` abu') abu'
  where abu' = takeWhile (<(28123 - n)) abudant

main = do
  -- putStrLn . show $ sum . takeWhile (< 28123) . filter (not . isAbundant) $ [1..]
  print . take 100 . filter isAbundant $ [1..]
  print . take 100 . filter isAbuDouble $ [1..]
  -- putStrLn . show $ sum . filter (not . isAbuDouble) $ [1..28123]
  -- putStrLn . show $ filter (not . isAbuDouble) $ [1..28123]


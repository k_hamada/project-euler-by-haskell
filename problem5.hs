-- Problem 5
-- 2520 は 1 から 10 の数字の全ての整数で割り切れる数字であり、そのような数字の中では最小の値である。
-- では、1 から 20 までの整数全てで割り切れる数字の中で最小の値はいくらになるか。

-- http://tsumuji.cocolog-nifty.com/tsumuji/2009/12/project-euler-3.html

main = do
  print . foldl1 lcm $ [1 .. 10]
  print . foldl1 lcm $ [1 .. 20]


-- Problem 1
-- 10未満の自然数のうち、3 もしくは 5 の倍数になっているものは 3, 5, 6, 9 の4つがあり、 これらの合計は 23 になる。
-- 同じようにして、1,000 未満の 3 か 5 の倍数になっている数字の合計を求めよ。

main = do
  let example = [x | x <- [1..9], mod x 3 == 0 || mod x 5 == 0]
      question = [x | x <- [1..999], mod x 3 == 0 || mod x 5 == 0]
  print example
  print . sum $ example
  print question
  print . sum $ question


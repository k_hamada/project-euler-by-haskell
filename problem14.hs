-- Problem 14
-- 正の整数に以下の式で繰り返し生成する数列を定義する。
--   n → n/2 (n が偶数)
--   n → 3n + 1 (n が奇数)
--
-- 13からはじめるとこの数列は以下のようになる。
--   13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
-- 13から1まで10個の項になる。 この数列はどのような数字からはじめても最終的には 1 になると考えられているが、まだそのことは証明されていない(コラッツ問題)
--
-- さて、100万未満の数字の中でどの数字からはじめれば一番長い数列を生成するか。
--
-- 注意: 数列の途中で100万以上になってもよい

import Data.List (maximumBy)

-- http://www.haskell.org/haskellwiki/Memoization

type MemoTable a = [(Integer, BinTree a)]
data BinTree a = Leaf a | Node Integer (BinTree a) (BinTree a)

wonderous3 :: Integer -> Integer
wonderous3 x
  = searchMemoTable x memoTable
  where
        memoTable :: MemoTable Integer
        memoTable = buildMemoTable 1 5

        buildMemoTable n i
            = (nextn, buildMemoTable' n i) : buildMemoTable nextn (i+1)
            where
                nextn = n + 2^i

                buildMemoTable' base 0
                    = Leaf (wonderous3' base)
                buildMemoTable' base i
                    = Node (base + midSize)
                           (buildMemoTable' base (i-1))
                           (buildMemoTable' (base + midSize) (i-1))
                    where
                        midSize = 2 ^ (i-1)

        searchMemoTable x ((x',tree):ms)
            | x < x'    = searchMemoTree x tree
            | otherwise = searchMemoTable x ms

        searchMemoTree x (Leaf y) = y
        searchMemoTree x (Node mid l r)
            | x < mid   = searchMemoTree x l
            | otherwise = searchMemoTree x r

        wonderous3' 1 = 0
        wonderous3' x
          | even x    = 1 + wonderous3 (x `div` 2)
          | otherwise = 1 + wonderous3 (3*x+1)

main =
  print $ maximumBy (\(_,x)(_,y) -> compare x y) [(i, wonderous3 i) | i <- [1..100000]]


-- Problem 15
-- 2 × 2 のマス目の左上からスタートした場合、引き返しなしで右下にいくルートは 6 つある。
-- では、20 × 20 のマス目ではいくつのルートがあるか。

factorial :: Integer -> Integer
factorial n = product [1..n]

combination :: Integer -> Integer -> Integer
combination n m = factorial n `div` (factorial m * factorial (n - m))

main = do
  print $ combination (2 * 2) 2
  print $ combination (20 * 2) 20


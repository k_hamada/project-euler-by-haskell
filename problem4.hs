-- Problem 4
-- 左右どちらから読んでも同じ値になる数を回文数という。 2桁の数の積で表される回文数のうち、最大のものは 9009 = 91 × 99 である。
-- では、3桁の数の積で表される回文数のうち最大のものはいくらになるか。

palindrome :: Int -> Bool
palindrome n =
  show n == (reverse . show $ n)

main = do
  let n = 999
      question = [(i, j) | i <- [1..n], j <- [1..n]] -- http://d.hatena.ne.jp/n9d/20100803/1280819554
  print . maximum . filter palindrome . map (uncurry (*)) $ question


-- Problem 12
-- 三角数の数列は自然数の和で表わされ、7番目の三角数は 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28 である。 三角数の最初の10項は
--   1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
-- となる。
--
-- 最初の7項について、その約数を列挙すると、以下のとおり。
--    1: 1
--    3: 1,3
--    6: 1,2,3,6
--   10: 1,2,5,10
--   15: 1,3,5,15
--   21: 1,3,7,21
--   28: 1,2,4,7,14,28
-- これから、7番目の三角数である28は、6個以上の約数をもつ最初の三角数であることが分る。
--
-- では、501 個以上の約数をもつ最初の三角数はいくらか。

import Data.List (sort, group)
import Control.Arrow ((&&&))
import Data.Numbers.Primes

-- http://stackoverflow.com/questions/7803449/
divisorSize :: Int -> Int
divisorSize = product . map (succ . snd . (head &&& length)) . group . sort . primeFactors

triangularNumbers :: Int -> [Int] -> [Int]
triangularNumbers n (x:xs) =
  i : triangularNumbers i xs
  where i = n + x

main =
  print . head . snd . break (\i -> divisorSize i >= 501) . triangularNumbers 1 $ [2..]

